# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdoctools
pkgver=5.54.0
pkgrel=0
pkgdesc="Documentation generation from docbook"
arch="all"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1"
depends="docbook-xsl"
depends_dev="qt5-qtbase-dev ki18n-dev karchive-dev libxslt-dev libxml2-dev libxml2-utils docbook-xml docbook-xsl"
makedepends="$depends_dev extra-cmake-modules perl-uri"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/${pkgname}-${pkgver}.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"
options="!check" # Broken, complains about not being able to find "kdoctools catalogs"

build() {
	cmake \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DLIB_INSTALL_DIR=lib
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}


package() {
	cd "$builddir"
	make DESTDIR="${pkgdir}" install
}

sha512sums="b389fd1a7b2ea32d13ddca128ed2cb6375856f1c40f83c29a46e9ed1713de6e28e4415743403de37cf1f8a4ddcba4d7cc1e295e5c50443c4faf02939854f72cb  kdoctools-5.54.0.tar.xz"
